package lexer

import (
	"testing"

	"gitlab.com/kevin42/monkey/token"
)

func TestReadChar(t *testing.T) {
	l := New("hello")
	l.readChar()
	if l.ch == 0 {
		t.Fatalf("You loose")
	}
	if l.position == 0 {
		t.Fatalf("You loose")
	}
}

func TestNextToken(t *testing.T) {
	input := `let five = 5;
    let ten = 10;
    
    let add = fn(x, y) {
        x + y
    };
    
    let result = add(five, ten);
    `

	tests := []struct {
		expectedType    token.TokenType
		expectedLiteral string
	}{
		{token.LET, "let"},
		{token.IDENT, "five"},
		{token.ASSIGN, "="},
		{token.INT, "5"},
		{token.SEMICOLON, ";"},
	}

	l := New(input)

	for i, tt := range tests {
		tok := l.NextToken()

		if tok.Type != tt.expectedType {
			t.Fatalf("test[%d] - tokentype wrong. expected=%q, got %q",
				i, tt.expectedType, tok.Type)
		}

		if tok.Literal != tt.expectedLiteral {
			t.Fatalf("test[%d] - literal wrong. expected=%q, got %q",
				i, tt.expectedLiteral, tok.Literal)
		}
	}
}
